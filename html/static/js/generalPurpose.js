window.addEventListener('load', (evt)=>{
    let content;
    if(page){
        switch(page){
            case 'upper':
                content = document.getElementById('general-bottom-container');
                break;
            case 'lower':
                content = document.getElementById('general-top-container');
                break;
            default:
                break;
        }
        if(content) content.style.display = 'none';
    }
    if(container){
        const containerController = document.getElementById('container-controller');
        switch(container){
            case 'pet':
                containerController.classList.remove('container-md');
                containerController.classList.add('container-fluid');
                break;
            default:
                break;
        }
    }
    const randomTxt = document.getElementById("index-random-lower");
    if(randomTxt){
        const randomTexts = [
            "hyperactive",
            "annoying",
            "not safe for normies",
            "working from home",
            "impossible",
            "lovely",
            "explosive",
            "petting",
            "town",
            "^~^",
            "you have clicked 100000 times",
            "taking over the internet",
            "shiny!",
            "bonjour!",
            "hi again!"
        ];
        randomTxt.innerText = randomTexts[Math.floor(Math.random() * randomTexts.length)];
    }
});