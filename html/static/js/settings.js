const nameMainSettings = document.getElementById('settings-name-change');
const imageMainSettings = document.getElementById('settings-image-change');
if(nameMainSettings){
    // console.log('asd');
    function countChars(place){
        const nameInput = document.getElementById('settings-name-input');
        let counter = (32 - (nameInput.value.length));
        const nameCount = document.getElementById('settings-name-used');
        nameCount.innerHTML = counter;
        if(nameInput.value.length == nameInput.maxLength){
            nameInput.classList.add('errormsg-error');
        }
        else{
            nameInput.classList.remove('errormsg-error');
        }
    }
    const nameInput = document.getElementById('settings-name-input');
    if(nameInput){
        // const nameCount = document.getElementById('settings-name-used');
        const nameValue = nameInput.value;
        // nameCount.innerHTML = nameValue.length;
        nameInput.addEventListener('keyup', countChars, false);
        if(nameValue == nameInput.maxLength){
            nameInput.style.display = 'none';
            nameInput.classList.add('errormsg-error');
        }
    }
    nameMainSettings.addEventListener('submit', function(evt){
        evt.preventDefault();
        const errorMsg = document.getElementById('settings-name-submit');
        if(!document.getElementById('settings-name-consent').checked){
            errorMsg.innerHTML = 'You need to consent that your name is <em>NOT</em> offenssive.'
            errorMsg.classList.add('errormsg-error');
            const timeout = setTimeout(function(){
                errorMsg.innerHTML = 'SUBMIT';
                errorMsg.classList.remove('errormsg-error');
                window.clearTimeout(timeout);
            }, 5000);
            return;
        }
        else{
            errorMsg.classList.remove('errormsg-error');
            const result = document.getElementById('settings-name-input');
            if(result.value == ''){
                errorMsg.innerHTML = 'Your display name can not be empty.';
                errorMsg.classList.add('errormsg-error');
                const timeout = setTimeout(function(){
                    errorMsg.innerHTML = 'SUBMIT';
                    errorMsg.classList.remove('errormsg-error');
                    window.clearTimeout(timeout);
                }, 5000);
                return;
            }
            else{
                const xhr = new XMLHttpRequest();
                xhr.open("POST", "/settings/name");
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === xhr.DONE) {
                        if (xhr.status === 200) {
                            const responseData = JSON.parse(xhr.response);
                            if (responseData.success) {
                                errorMsg.innerHTML = 'Successfully updated your name';
                                errorMsg.classList.add('errormsg-success');
                                document.getElementById('settings-current-name').innerText = result.value;
                                const timeout = setTimeout(function(){
                                    errorMsg.innerHTML = 'SUBMIT';
                                    errorMsg.classList.remove('errormsg-success');
                                    window.clearTimeout(timeout);
                                }, 5000);
                            } else {
                                errorMsg.innerHTML = 'Unable to update name: ' + responseData.message;
                                errorMsg.classList.add('errormsg-error');
                                const timeout = setTimeout(function(){
                                    errorMsg.innerHTML = 'SUBMIT';
                                    errorMsg.classList.remove('errormsg-error');
                                    window.clearTimeout(timeout);
                                }, 5000);
                            }
                        }
                    }
                }
                xhr.onerror = function(event){
                    errorMsg.innerHTML = 'Unable to reach server';
                    errorMsg.classList.add('errormsg-error');
                    const timeout = setTimeout(function(){
                        errorMsg.innerHTML = 'SUBMIT';
                        errorMsg.classList.remove('errormsg-error');
                        window.clearTimeout(timeout);
                    }, 5000);
                    return;
                }
                const formData = new FormData(document.getElementById('settings-name-change'));
                xhr.send(formData);
            }
        }
    });
}
if(imageMainSettings){
    const imageInput = document.getElementById('settings-image-input');
    let fileName;
    imageInput.addEventListener('change', (event)=>{
        fileName = imageInput.files[0];
        if(fileName){
            const label = document.getElementById('settings-image-input-label');
            label.innerHTML = fileName.name + ' chosen';
        }
    });
    imageMainSettings.addEventListener('submit', function(evt){
        evt.preventDefault();
        const errorMsg = document.getElementById('settings-image-submit');
        // const currentName = document.getElementById('settings-current-name');
        // let currentnameName;
        // let currentNameTemp;
        // if(!currentName){
        //     currentNameTemp = document.createElement('textarea');
        //     currentNameTemp.style.position = 'absolute';
        //     currentNameTemp.style.left = '-999999px';
        //     currentNameTemp.setAttribute('readonly','');
        //     const nameChange = document.getElementById('settings-name-change');
        //     nameChange.appendChild(currentNameTemp);
        //     nameChange.setAttribute('id', 'name-temp');
        //     currentnameName = document.getElementById('name-temp');
        // }
        // else{
        //     currentnameName = currentName;
        // }
        if(document.getElementById('settings-current-name').innerText.trim() == ''){
            errorMsg.innerHTML = 'You need to set a name first, before you can add an image!';
            errorMsg.classList.add('errormsg-error');
            const timeout = setTimeout(function(){
                errorMsg.innerHTML = 'SUBMIT';
                errorMsg.classList.remove('errormsg-error');
                window.clearTimeout(timeout);
            }, 5000);
            return;
        }

        if(!document.getElementById('settings-image-consent').checked){
            errorMsg.innerHTML = 'You need to consent that we can use this image on this website.';
            errorMsg.classList.add('errormsg-error');
            const timeout = setTimeout(function(){
                errorMsg.innerHTML = 'SUBMIT';
                errorMsg.classList.remove('errormsg-error');
                window.clearTimeout(timeout);
            }, 5000);
            return;
        }
        else{
            errorMsg.classList.remove('errormsg-error');
        }

        if(!document.getElementById('settings-image-nsfw').checked){
            errorMsg.innerHTML = 'You need to consent that your image is not NSFW, or offenssive.';
            errorMsg.classList.add('errormsg-error');
            const timeout = setTimeout(function(){
                errorMsg.innerHTML = 'SUBMIT';
                errorMsg.classList.remove('errormsg-error');
                window.clearTimeout(timeout);
            }, 5000);
            return;
        }
        else{
            errorMsg.classList.remove('errormsg-error');
        }

        errorMsg.classList.remove('errormsg-error');
        const result = document.getElementById('settings-image-input');
        if(result.value == ''){
            errorMsg.innerHTML = 'Your image can not be empty.';
            errorMsg.classList.add('errormsg-error');
            const timeout = setTimeout(function(){
                errorMsg.innerHTML = 'SUBMIT';
                errorMsg.classList.remove('errormsg-error');
                window.clearTimeout(timeout);
            }, 5000);
            return;
        }
        else{
            const xhr = new XMLHttpRequest();
            xhr.open("POST", "/settings/image");
            if(xhr.upload){
                // console.log(fileName);
                // const bar = document.getElementById('progress-bar');
                // const label = document.getElementById('settings-image-input-label');
                // const progress = bar.appendChild(document.createElement('p'));
                errorMsg.innerHTML = 'Upload: ' + fileName.name;
                xhr.upload.addEventListener('progress', function(e){
                    if(e.lengthComputable){
                        const progressCount = parseInt(100 - (e.loaded / e.total * 100));
                        console.log(progressCount);
                        // label.style.backgroundPosition = progressCount + "% 0";
                    }
                }, false);
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === xhr.DONE) {
                        if (xhr.status === 200) {
                            const responseData = JSON.parse(xhr.response);
                            // console.log(sresponseData);
                            if (responseData.success) {
                                errorMsg.innerHTML = 'Uploaded: ' + fileName.name;
                                errorMsg.classList.add('errormsg-success');
                                document.getElementById('settings-current-image').src = responseData.image;
                                const timeout = setTimeout(function(){
                                    errorMsg.innerHTML = 'SUBMIT';
                                    errorMsg.classList.remove('errormsg-success');
                                    window.clearTimeout(timeout);
                                }, 5000);
                            } else {
                                errorMsg.innerHTML = 'Failed to upload: ' + fileName.name + ' | ' + responseData.message;
                                errorMsg.classList.remove('errormsg-success');
                                errorMsg.classList.add('errormsg-error');
                                const timeout = setTimeout(function(){
                                    errorMsg.innerHTML = 'SUBMIT';
                                    errorMsg.classList.remove('errormsg-error');
                                    window.clearTimeout(timeout);
                                }, 5000);
                                // errorMsg.innerHTML = 'Unable to update image: ' + responseData.message;
                                // errorMsg.classList.add('errormsg-error');
                            }
                        }
                    }
                }
            }
            xhr.onerror = function(event){
                errorMsg.innerHTML = 'Unable to reach server';
                errorMsg.classList.add('errormsg-error');
                const timeout = setTimeout(function(){
                    errorMsg.innerHTML = 'SUBMIT';
                    errorMsg.classList.remove('errormsg-error');
                    window.clearTimeout(timeout);
                }, 5000);
            }
            const formData = new FormData(document.getElementById('settings-image-change'));
            xhr.send(formData);
        }
        document.removeChild(currentNameTemp);
    });
}
