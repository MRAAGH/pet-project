package accountcollection

import (
	"log"
	"petproject/account"
)

var collection map[int]*account.Account = nil

func init() {
	if collection != nil {
		return
	}
	collection = make(map[int]*account.Account)
}

func Get(id int) *account.Account {
	for i, a := range collection {
		if i == id {
			return a
		}
	}
	log.Print("Fetching account from DB")
	a := account.FromDbId(id)
	if a == nil {
		return nil
	}
	collection[id] = a
	return a
}

func GetByPet(id int) *account.Account {
	// for now it is the same as Get
	return Get(id)
}

func GetAuth(method account.LoginMethod, auth string) *account.Account {
	for _, a := range collection {
		if a.IsAuth(method, auth) {
			return a
		}
	}
	log.Print("Fetching account from DB")
	a := account.FromDbAuth(method, auth)
	if a == nil {
		return nil
	}
	collection[a.GetId()] = a
	return a
}

func GetOrCreateAuth(method account.LoginMethod, auth string) *account.Account {
	a := GetAuth(method, auth)
	if a != nil {
		return a
	}
	a = account.New(auth)
	a.SetAuth(method, auth)
	collection[a.GetId()] = a
	return a
}

func Save() {
	for _, a := range collection {
		a.Save()
	}
}

func Maintenance() {
	ids := account.Maintenance()
	for _, id := range ids {
		delete(collection, id)
	}
}
