package petcollection

import (
	"log"
	"petproject/pet"
	"petproject/util"
	"math/rand"
)

var collection map[int]*pet.Pet = nil

func init() {
	if collection != nil {
		return
	}
	collection = make(map[int]*pet.Pet)
}

func GetRandomTemplates(num int) []*pet.TemplatePet {
	colLen := len(collection)
	retLen := util.Min(colLen, num)
	ret := make([]*pet.TemplatePet, retLen, retLen)
	if colLen <= num {
		idx := 0
		for _, p := range collection {
			ret[idx] = p.GetTemplate()
			idx++
		}
		return ret
	}
	idx := 0
	haveIdx := make([]int, retLen, retLen)
	// we initialize all the random indexes to -1
	for i := range haveIdx {
		haveIdx[i] = -1
	}
	for idx < retLen {
		// get a random map offset
		iMap := rand.Intn(colLen)
		i := 0 // and convert the map offset to an actual pet id
		for ii, p := range collection {
			if iMap == 0 {
				i = ii
				ret[idx] = p.GetTemplate()
				break
			}
			iMap--
		}
		// check if the pet id exists...
		if util.Contains(haveIdx, i) {
			continue
		}
		haveIdx[idx] = i
		idx++
	}
	return ret
}


func Get(id int) *pet.Pet {
	for i, p := range collection {
		if i == id {
			return p
		}
	}
	log.Print("Fetching pet from DB")
	p := pet.FromDB(id)
	if p == nil {
		return nil
	}
	collection[id] = p
	return p
}

func GetFromUrl(url string) *pet.Pet {
	for _, p := range collection {
		if url == p.GetUrl() {
			return p
		}
	}
	log.Print("Fetching pet from DB")
	p := pet.FromDBUrl(url)
	if p == nil {
		return nil
	}
	collection[p.GetId()] = p
	return p
}

func GetOrCreate(id int, name string) *pet.Pet {
	p := Get(id)
	if p != nil {
		return p
	}
	p = pet.New(id, name)
	collection[id] = p
	return p
}

func Save() {
	for _, p := range collection {
		p.Save()
	}
}
